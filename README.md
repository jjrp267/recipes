Servicio Api/Rest con symfony 3.3
=================================

#datos de configuración
Instalado el bundle FOSRESTBUNDLE
datos iniciales de recetas en app/db/article.sql

Forma de acceder a los datos de las recetas:

1).-Obtener una receta:

http://localhost/servicio33/web/app_dev.php/api/article/{id}

retorna los datos de la receta en formato json

2).-Obtener todas las recetas ()

http://localhost/servicio33/web/app_dev.php/api/articles

muestra listado de todas las recetas en formato html.
Maquetado en bootstrap 3.3.7

[screenshot]

[screenshot]: listadorecetas.png
